package zilch.portfolio.domain;

import java.util.List;

// Object class contain profile information
public class Profile {

	public static final int PROFILE_DEFAULT_ID = 1;
	
	private int id;
	private String firstname;
	private String middlename;
	private String lastname;
	private String address;
	private String website;
	private List<Contact> telephoneList;
	private List<Contact> emailList;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the middlename
	 */
	public String getMiddlename() {
		return middlename;
	}
	/**
	 * @param middlename the middlename to set
	 */
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the full name
	 */
	public String getName(){
		String fullname = "";
		fullname += firstname;
		if(!middlename.isEmpty()){
			fullname += ' '+middlename.toUpperCase().charAt(0)+'.';
		}
		fullname += ' '+lastname;
		return fullname;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/**
	 * @return the telephoneList
	 */
	public List<Contact> getTelephoneList() {
		return telephoneList;
	}
	/**
	 * @param telephoneList the telephoneList to set
	 */
	public void setTelephoneList(List<Contact> telephoneList) {
		this.telephoneList = telephoneList;
	}
	/**
	 * @return the emailList
	 */
	public List<Contact> getEmailList() {
		return emailList;
	}
	/**
	 * @param emailList the emailList to set
	 */
	public void setEmailList(List<Contact> emailList) {
		this.emailList = emailList;
	}
	
	
}
