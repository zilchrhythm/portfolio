package zilch.portfolio.database;

import java.util.ArrayList;
import java.util.List;

import zilch.portfolio.domain.Contact;
import zilch.portfolio.domain.Profile;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHandler extends SQLiteOpenHelper {
	
	static final String dbname = "profileDB"; // DB name
	
	// Profile table columns' name
	static final String profileTableName = "PROFILE";
	static final String profile_id = "PROFILEID";
	static final String profile_firstName = "FIRSTNAME";
	static final String profile_middleName = "MIDDLENAME";
	static final String profile_lastname = "LASTNAME";
	static final String profile_address = "ADDRESS";
	static final String profile_website = "WEBSITE";
	
	// Telephone and mail table columns' name
	static final String telephoneTablename = "TELEPHONE";
	static final String emailTableName = "EMAIL";
	static final String value = "VALUE";
	static final String type = "TYPE";

	public DBHandler(Context context){
		super(context, dbname, null, 1);
	}
	
	public DBHandler(Context context, String name, CursorFactory factory,
			int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create Profile table
		db.execSQL("CREATE TABLE " + profileTableName+ " ("
				+ profile_id + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ profile_firstName + " VARCHAR(30),"
				+ profile_middleName + " VARCHAR(30),"
				+ profile_lastname + " VARCHAR(40),"
				+ profile_address + " VARCHAR(200),"
				+ profile_website + " VARCHAR(50)"
				+ ")");
		
		// Create Telephone table
		db.execSQL("CREATE TABLE " + telephoneTablename+ "("
				+ profile_id + " INTEGER,"
				+ value + " VARCHAR(20),"
				+ type + " VARCHAR(20)"
				+ ")");
		
		// Create Email table
		db.execSQL("CREATE TABLE " + emailTableName+ "("
				+ profile_id + " INTEGER,"
				+ value + " VARCHAR(50),"
				+ type + " VARCHAR(20)"
				+ ")");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	// Add profile info into db
	public void addProfile(Profile profile){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(profile_firstName,profile.getFirstname());
		values.put(profile_middleName, profile.getMiddlename());
		values.put(profile_lastname, profile.getFirstname());
		values.put(profile_address, profile.getAddress());
		values.put(profile_website, profile.getWebsite());
		
		db.insert(profileTableName, null, values);
		db.close();
		
		addTelephone(profile);
		addEmail(profile);
	}
	
	// Add profile's telephone info into db
	public void addTelephone(Profile profile){
		
		List<Contact> telephoneList = profile.getTelephoneList();
		if(!telephoneList.isEmpty()){
			SQLiteDatabase db = this.getWritableDatabase();
			
			for(Contact contact:telephoneList){
				ContentValues values = new ContentValues();
				values.put(profile_id,profile.getId());
				values.put(value, contact.getValue());
				values.put(type, contact.getType());
				db.insert(telephoneTablename, null, values);
			}
			
			db.close();
		}
	}
	
	// Add profile's telephone info into db
	public void addEmail(Profile profile){
		
		List<Contact> emailList = profile.getEmailList();
		if(!emailList.isEmpty()){
			SQLiteDatabase db = this.getWritableDatabase();
			
			for(Contact contact:emailList){
				ContentValues values = new ContentValues();
				values.put(profile_id, profile.getId());
				values.put(value, contact.getValue());
				values.put(type, contact.getType());
				db.insert(emailTableName, null, values);
			}
			
			db.close();
		}
	}	

	// Update profile info to db
	public void updateProfile(Profile profile){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(profile_firstName,profile.getFirstname());
		values.put(profile_middleName, profile.getMiddlename());
		values.put(profile_lastname, profile.getFirstname());
		values.put(profile_address, profile.getAddress());
		values.put(profile_website, profile.getWebsite());
		
		db.update(profileTableName, values, profile_id+"=?", new String[]{String.valueOf(profile.getId())});
		db.close();
	}

	// Update profile's telephone info to db
	public void updateTelephone(Profile profile){
		deleteTelephone(profile);
		addTelephone(profile);
	}
	
	// Update profile's email info to db
	public void updateEmail(Profile profile){
		deleteEmail(profile);
		addEmail(profile);
	}
	
	// Delete profile's telephone info from db
	public void deleteTelephone(Profile profile){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(telephoneTablename, profile_id+"=?", new String[]{String.valueOf(profile.getId())});
		db.close();
	}
	
	// Delete profile's email info from db
	public void deleteEmail(Profile profile){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(emailTableName, profile_id+"=?", new String[]{String.valueOf(profile.getId())});
		db.close();
	}

	// Read profile info from db
	public Profile getProfile(int id){
		Profile profile = new Profile();
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(profileTableName, 
								new String[]{profile_id,profile_firstName,profile_middleName,profile_lastname,profile_address,profile_website}, 
								profile_id+"=?", new String[]{String.valueOf(id)}, null, null, null);
		if(cursor!=null && cursor.moveToFirst()){
			profile.setId(cursor.getInt(cursor.getColumnIndex(profile_id)));
			profile.setFirstname(cursor.getString(cursor.getColumnIndex(profile_firstName)));
			profile.setMiddlename(cursor.getString(cursor.getColumnIndex(profile_middleName)));
			profile.setLastname(cursor.getString(cursor.getColumnIndex(profile_lastname)));
			profile.setAddress(cursor.getString(cursor.getColumnIndex(profile_address)));
			profile.setWebsite(cursor.getString(cursor.getColumnIndex(profile_website)));
		}		
		db.close();
		
		return profile;
	}
	
	// Read profile's telephone info from db
	public List<Contact> getTelephone(int id){
		List<Contact> telephoneList = new ArrayList<Contact>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(telephoneTablename, 
								new String[]{value,type}, 
								profile_id+"=?", new String[]{String.valueOf(id)}, null, null, null);
		if(cursor!=null && cursor.moveToFirst()){
			do{
				Contact contact = new Contact();
				contact.setValue(cursor.getString(cursor.getColumnIndex(value)));
				contact.setType(cursor.getString(cursor.getColumnIndex(type)));
				telephoneList.add(contact);
			}while(cursor.moveToNext());
		}
		
		return telephoneList;
	}
	
	// Read profile's email info from db
	public List<Contact> getEmail(int id){
		List<Contact> emailList = new ArrayList<Contact>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(emailTableName, 
								new String[]{value,type}, 
								profile_id+"=?", new String[]{String.valueOf(id)}, null, null, null);
		if(cursor!=null && cursor.moveToFirst()){
			do{
				Contact contact = new Contact();
				contact.setValue(cursor.getString(cursor.getColumnIndex(value)));
				contact.setType(cursor.getString(cursor.getColumnIndex(type)));
				emailList.add(contact);
			}while(cursor.moveToNext());
		}
		
		return emailList;
	}
}
