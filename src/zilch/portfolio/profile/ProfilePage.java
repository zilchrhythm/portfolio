package zilch.portfolio.profile;

import zilch.portfolio.R;
import zilch.portfolio.database.DBHandler;
import zilch.portfolio.domain.Profile;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProfilePage extends Activity{
	Button menu_btn;
	boolean menu_visible = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_page);
		
		DBHandler db = new DBHandler(this);
		Profile profile = db.getProfile(Profile.PROFILE_DEFAULT_ID);
		TextView tv = (TextView) findViewById(R.id.profile_name);
		tv.setText(profile.getName());
		
		menu_btn = (Button) findViewById(R.id.profile_btn_menu);
		menu_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				LinearLayout menu = (LinearLayout) findViewById(R.id.profile_menu);
				if(menu_visible){
					menu.setVisibility(View.INVISIBLE);
					menu_visible = false;
				}else{
					menu.setVisibility(View.VISIBLE);
					menu_visible = true;
				}
			}
		});
		
	}
}