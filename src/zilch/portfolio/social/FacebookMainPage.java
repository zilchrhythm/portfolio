package zilch.portfolio.social;

import zilch.portfolio.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.widget.TextView;

import com.facebook.*;
import com.facebook.model.*;

public class FacebookMainPage extends Activity {
	
	TextView textView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facebook_main_page);
		
		Session.openActiveSession(this, true, new Session.StatusCallback() {
			
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if(session.isOpened()){
					Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
						
						@Override
						public void onCompleted(GraphUser user, Response response) {
							if(user != null){
								TextView welcome = (TextView) findViewById(R.id.textview_facebook_main_page);
								welcome.setText("Hello " + user.getName());
							}
						}
					});
				}
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
}
